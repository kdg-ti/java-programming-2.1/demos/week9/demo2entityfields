package be.kdg.java2.demo2entityfields;

public enum Genre {
    THRILLER, BIOGRAPHY, FANTASY
}
