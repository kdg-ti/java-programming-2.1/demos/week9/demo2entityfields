package be.kdg.java2.demo2entityfields;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name="books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title", nullable = false, length = 50)
    private String title;

    @Column(name = "pages")
    private int pages;

    @Column(name = "pub_date")
    private LocalDate datePublished;

    @Column(name = "best_reading_time")
    private LocalTime bestReadingTime;

    @Enumerated(EnumType.STRING)
    private Genre genre;

    protected Book() {
    }

    public Book(String title, int pages, LocalDate datePublished, LocalTime bestReadingTime, Genre genre) {
        this.title = title;
        this.pages = pages;
        this.datePublished = datePublished;
        this.bestReadingTime = bestReadingTime;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public LocalTime getBestReadingTime() {
        return bestReadingTime;
    }

    public void setBestReadingTime(LocalTime bestReadingTime) {
        this.bestReadingTime = bestReadingTime;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", datePublished=" + datePublished +
                ", bestReadingTime=" + bestReadingTime +
                ", genre=" + genre +
                '}';
    }
}
