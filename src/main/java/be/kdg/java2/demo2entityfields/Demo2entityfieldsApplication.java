package be.kdg.java2.demo2entityfields;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo2entityfieldsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo2entityfieldsApplication.class, args);
    }

}
